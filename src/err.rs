//! Error handling.

use std::borrow::Cow;
use thiserror::Error;
use untrusted::EndOfInput;

#[derive(Error, Debug)]
pub enum ParseError {
    #[error("encountered unexpected end of input")]
    Incomplete,
    #[error("unknown error from the untrustended crate")]
    Untrustended,
    #[error("invalid value {value} for {ty}")]
    InvalidValue { value: usize, ty: &'static str },
    #[error("invalid IKE version")]
    VersionMismatch,
    #[error("invalid SA proposal number {provided}, expected {expected}")]
    ProposalNumMismatch { provided: u8, expected: u8 },
    #[error("length/count mismatch inside {0}")]
    LengthMismatch(&'static str),
    #[error("inside {substructure}: {error}")]
    Inside {
        substructure: Cow<'static, str>,
        error: Box<ParseError>,
    },
}

impl ParseError {
    pub(crate) fn invalid_for(value: impl Into<usize>, ty: &'static str) -> Self {
        Self::InvalidValue {
            value: value.into(),
            ty,
        }
    }
}

impl From<EndOfInput> for ParseError {
    fn from(_: EndOfInput) -> Self {
        Self::Incomplete
    }
}

impl From<untrustended::Error> for ParseError {
    fn from(value: untrustended::Error) -> Self {
        use untrustended::Error::*;
        match value {
            EndOfInput => Self::Incomplete,
            _ => Self::Untrustended,
        }
    }
}

pub type ParseResult<T> = std::result::Result<T, ParseError>;

pub trait InsideExt<T> {
    fn err_inside(self, substructure: impl Into<Cow<'static, str>>) -> Result<T, ParseError>;
}
impl<T> InsideExt<T> for ParseResult<T> {
    fn err_inside(self, substructure: impl Into<Cow<'static, str>>) -> Result<T, ParseError> {
        match self {
            Ok(v) => Ok(v),
            Err(e) => Err(ParseError::Inside {
                substructure: substructure.into(),
                error: Box::new(e),
            }),
        }
    }
}
impl<T> InsideExt<T> for Result<T, untrustended::Error> {
    fn err_inside(self, substructure: impl Into<Cow<'static, str>>) -> Result<T, ParseError> {
        self.map_err(|e| ParseError::from(e))
            .err_inside(substructure)
    }
}
