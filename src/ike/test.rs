//! IKE message testing.

use crate::dh::DiffieKey2048;
use crate::ike::consts::{
    DhTransform, EncrTransform, IkeExchangeType, IntegTransform, IpsecProtocol, NotificationType,
    PrfTransform,
};
use crate::ike::sa::{IkeSaProposal, IkeSaTransform};
use crate::ike::{
    IkeKeyExchange, IkeMessage, IkeNotify, IkeNotifyData, IkePayload, IkeTrafficSelector,
};
use rand::RngCore;

#[test]
fn roundtrip_sa() {
    let message = IkeMessage::sa_init().with_payload(IkePayload::new_sa(vec![IkeSaProposal::new(
        IpsecProtocol::Ike,
    )
    .with_encr(EncrTransform::ENCR_AES_CBC, Some(128))
    .with_prf(PrfTransform::PRF_HMAC_SHA1)
    .with_integ(IntegTransform::AUTH_HMAC_SHA1_96)
    .with_dh(DhTransform::MODP_2048_bit)]));

    let bytes = message.as_bytes().unwrap();
    let parsed_message = IkeMessage::parse(&bytes).unwrap();
    assert_eq!(parsed_message, message);
}

#[test]
fn roundtrip_ts() {
    let message = IkeMessage::sa_init()
        .with_payload(IkePayload::new_tsi(vec![
            IkeTrafficSelector::Ipv4 {
                protocol: 6,
                start: "1.2.3.4:42".parse().unwrap(),
                end: "3.4.5.6:123".parse().unwrap(),
            },
            IkeTrafficSelector::Ipv6 {
                protocol: 6,
                start: "[fe80::1]:42".parse().unwrap(),
                end: "[fe80::2]:123".parse().unwrap(),
            },
        ]))
        .with_payload(IkePayload::new_tsr(vec![IkeTrafficSelector::Ipv4 {
            protocol: 6,
            start: "100.200.200.100:1111".parse().unwrap(),
            end: "2.2.2.2:222".parse().unwrap(),
        }]));

    let bytes = message.as_bytes().unwrap();
    let parsed_message = IkeMessage::parse(&bytes).unwrap();
    assert_eq!(parsed_message, message);
}

#[test]
fn roundtrip_full() {
    let dh_key = DiffieKey2048::new();
    let mut nonce = vec![0u8; 256];
    rand::thread_rng().fill_bytes(&mut nonce);

    let message = IkeMessage::sa_init()
        .with_payload(IkePayload::new_sa(vec![IkeSaProposal::new(
            IpsecProtocol::Ike,
        )
        .with_encr(EncrTransform::ENCR_AES_CBC, Some(128))
        .with_prf(PrfTransform::PRF_HMAC_SHA1)
        .with_integ(IntegTransform::AUTH_HMAC_SHA1_96)
        .with_dh(DhTransform::MODP_2048_bit)]))
        .with_payload(IkePayload::new_ke(
            DhTransform::MODP_2048_bit,
            dh_key.public_key().into(),
        ))
        .with_payload(IkePayload::new_nonce(nonce));

    let bytes = message.as_bytes().unwrap();
    let parsed_message = IkeMessage::parse(&bytes).unwrap();
    assert_eq!(parsed_message, message);
}

#[test]
fn invalid_ke_payload() {
    let payload =
        hex::decode("2d0aa2beacb12f1200000000000000002920222000000000000000260000000a00000011000e")
            .unwrap();
    assert_eq!(
        IkeMessage::parse(&payload).unwrap(),
        IkeMessage {
            initiator_spi: [45, 10, 162, 190, 172, 177, 47, 18],
            responder_spi: [0, 0, 0, 0, 0, 0, 0, 0],
            version_major: 2,
            version_minor: 0,
            exchange_type: IkeExchangeType::SaInit,
            flag_response: true,
            flag_version: false,
            flag_initiator: false,
            message_id: 0,
            payloads: vec![IkePayload::Notify(IkeNotify {
                protocol: None,
                spi: vec![],
                data: IkeNotifyData::InvalidKePayload {
                    accepted_group: DhTransform::MODP_2048_bit
                },
            })]
        }
    );
}

#[test]
fn nat_detection() {
    let payload = hex::decode("2d0aa2beacb12f125c4bb7037a57510f2120222000000000000001a0220000300000002c010100040300000c0100000c800e008003000008020000020300000803000002000000080400000e28000108000e0000da386d2b57521c69ac33d60fd211504603796f9d676d7719783f7678919fec0b3497218eb5f8e898eb0f1f880bf66818319ced3e6fed5d9025f4e2cf82a94113831ec39e4d4e9cf6e1ce0221ef7e7493a8e758fbe058ddc7213fa097e4ef007f581bd6a2f3307829392f1125344136e1e32703adce27547bf528a1bd0f615e045c57c79f1b74f08647cbbb988b3971667d9593dba7f36e56ac4dfa03644e325682403625a86e77bed00241d8749b2ac45839880dd91b8b1d2638a863396fdcf6adecef1f12e0564f74c10ecf48f0558fe2e3d30d9a4fcc136e31c785fd16095026debaf26d4969a773b4d89b9496a88d3a1c270281c547582a393772749bc0e4290000148093a7df1cacaf820de4ef56b6e31b8d2900001c00004004d39f7e3667690ce3449b0dd338a68323dcc558d50000001c000040054ae8a53027e4b2ece2770d41dd5bfbb86e183043").unwrap();
    assert_eq!(
        IkeMessage::parse(&payload).unwrap(),
        IkeMessage {
            initiator_spi: [45, 10, 162, 190, 172, 177, 47, 18],
            responder_spi: [92, 75, 183, 3, 122, 87, 81, 15],
            version_major: 2,
            version_minor: 0,
            exchange_type: IkeExchangeType::SaInit,
            flag_response: true,
            flag_version: false,
            flag_initiator: false,
            message_id: 0,
            payloads: vec![
                IkePayload::SecurityAssociation(vec![IkeSaProposal {
                    protocol: IpsecProtocol::Ike,
                    spi: vec![],
                    transforms: vec![
                        IkeSaTransform::Encr {
                            ty: EncrTransform::ENCR_AES_CBC,
                            key_length: Some(128)
                        },
                        IkeSaTransform::Prf(PrfTransform::PRF_HMAC_SHA1),
                        IkeSaTransform::Integ(IntegTransform::AUTH_HMAC_SHA1_96),
                        IkeSaTransform::Dh(DhTransform::MODP_2048_bit)
                    ]
                }]),
                IkePayload::KeyExchange(IkeKeyExchange {
                    group: DhTransform::MODP_2048_bit,
                    dh_public: vec![
                        218, 56, 109, 43, 87, 82, 28, 105, 172, 51, 214, 15, 210, 17, 80, 70, 3,
                        121, 111, 157, 103, 109, 119, 25, 120, 63, 118, 120, 145, 159, 236, 11, 52,
                        151, 33, 142, 181, 248, 232, 152, 235, 15, 31, 136, 11, 246, 104, 24, 49,
                        156, 237, 62, 111, 237, 93, 144, 37, 244, 226, 207, 130, 169, 65, 19, 131,
                        30, 195, 158, 77, 78, 156, 246, 225, 206, 2, 33, 239, 126, 116, 147, 168,
                        231, 88, 251, 224, 88, 221, 199, 33, 63, 160, 151, 228, 239, 0, 127, 88,
                        27, 214, 162, 243, 48, 120, 41, 57, 47, 17, 37, 52, 65, 54, 225, 227, 39,
                        3, 173, 206, 39, 84, 123, 245, 40, 161, 189, 15, 97, 94, 4, 92, 87, 199,
                        159, 27, 116, 240, 134, 71, 203, 187, 152, 139, 57, 113, 102, 125, 149,
                        147, 219, 167, 243, 110, 86, 172, 77, 250, 3, 100, 78, 50, 86, 130, 64, 54,
                        37, 168, 110, 119, 190, 208, 2, 65, 216, 116, 155, 42, 196, 88, 57, 136,
                        13, 217, 27, 139, 29, 38, 56, 168, 99, 57, 111, 220, 246, 173, 236, 239,
                        31, 18, 224, 86, 79, 116, 193, 14, 207, 72, 240, 85, 143, 226, 227, 211,
                        13, 154, 79, 204, 19, 110, 49, 199, 133, 253, 22, 9, 80, 38, 222, 186, 242,
                        109, 73, 105, 167, 115, 180, 216, 155, 148, 150, 168, 141, 58, 28, 39, 2,
                        129, 197, 71, 88, 42, 57, 55, 114, 116, 155, 192, 228
                    ]
                }),
                IkePayload::Nonce {
                    nonce: vec![
                        128, 147, 167, 223, 28, 172, 175, 130, 13, 228, 239, 86, 182, 227, 27, 141
                    ]
                },
                IkePayload::Notify(IkeNotify {
                    protocol: None,
                    spi: vec![],
                    data: IkeNotifyData::Unimplemented {
                        ty: NotificationType::NAT_DETECTION_SOURCE_IP,
                        data: vec![
                            211, 159, 126, 54, 103, 105, 12, 227, 68, 155, 13, 211, 56, 166, 131,
                            35, 220, 197, 88, 213
                        ]
                    }
                }),
                IkePayload::Notify(IkeNotify {
                    protocol: None,
                    spi: vec![],
                    data: IkeNotifyData::Unimplemented {
                        ty: NotificationType::NAT_DETECTION_DESTINATION_IP,
                        data: vec![
                            74, 232, 165, 48, 39, 228, 178, 236, 226, 119, 13, 65, 221, 91, 251,
                            184, 110, 24, 48, 67
                        ]
                    }
                })
            ]
        }
    );
}
