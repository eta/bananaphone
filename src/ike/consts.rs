//! Defined magic numbers for the IKEv2 protocol.

#![allow(non_camel_case_types)]

use num_derive::FromPrimitive;

#[derive(Copy, Clone, Debug, PartialEq, Eq, PartialOrd, Ord, FromPrimitive)]
#[repr(u8)]
pub(crate) enum IkePayloadType {
    SecurityAssociation = 33,
    KeyExchange = 34,
    IdentificationInitiator = 35,
    IdentificationResponder = 36,
    Certificate = 37,
    CertificateRequest = 38,
    Authentication = 39,
    Nonce = 40,
    Notify = 41,
    Delete = 42,
    VendorId = 43,
    TrafficSelectorInitiator = 44,
    TrafficSelectorResponder = 45,
    EncryptedAuthenticated = 46,
    Configuration = 47,
    ExtensibleAuthentication = 48,
}

#[derive(Copy, Clone, Debug, PartialEq, Eq, PartialOrd, Ord, FromPrimitive)]
#[repr(u8)]
pub(crate) enum IkeExchangeType {
    SaInit = 34,
    Auth = 35,
    CreateChildSa = 36,
    Informational = 37,
}

#[derive(Copy, Clone, Debug, PartialEq, Eq, PartialOrd, Ord, FromPrimitive)]
#[repr(u8)]
pub(crate) enum IpsecProtocol {
    Ike = 1,
    Ah = 2,
    Esp = 3,
}

#[derive(Copy, Clone, Debug, PartialEq, Eq, PartialOrd, Ord, FromPrimitive)]
#[repr(u8)]
pub(crate) enum IkeTransformType {
    Encr = 1,
    Prf = 2,
    Integ = 3,
    Dh = 4,
    Esn = 5,
}

#[derive(Copy, Clone, Debug, PartialEq, Eq, PartialOrd, Ord, FromPrimitive)]
#[repr(u16)]
pub(crate) enum EncrTransform {
    ENCR_DES_IV64 = 1,
    ENCR_DES = 2,
    ENCR_3DES = 3,
    ENCR_RC5 = 4,
    ENCR_IDEA = 5,
    ENCR_CAST = 6,
    ENCR_BLOWFISH = 7,
    ENCR_3IDEA = 8,
    ENCR_DES_IV32 = 9,
    ENCR_NULL = 11,
    ENCR_AES_CBC = 12,
    ENCR_AES_CTR = 13,
    ENCR_AES_CCM_8 = 14,
    ENCR_AES_CCM_12 = 15,
    ENCR_AES_CCM_16 = 16,
    ENCR_AES_GCM_8 = 18,
    ENCR_AES_GCM_12 = 19,
    ENCR_AES_GCM_16 = 20,
}

#[derive(Copy, Clone, Debug, PartialEq, Eq, PartialOrd, Ord, FromPrimitive)]
#[repr(u16)]
pub(crate) enum PrfTransform {
    PRF_HMAC_MD5 = 1,
    PRF_HMAC_SHA1 = 2,
    PRF_HMAC_TIGER = 3,
    PRF_AES128_XCBC = 4,
    PRF_HMAC_SHA2_256 = 5,
    PRF_HMAC_SHA2_384 = 6,
    PRF_HMAC_SHA2_512 = 7,
    PRF_AES128_CMAC = 8,
}

#[derive(Copy, Clone, Debug, PartialEq, Eq, PartialOrd, Ord, FromPrimitive)]
#[repr(u16)]
pub(crate) enum IntegTransform {
    NONE = 0,
    AUTH_HMAC_MD5_96 = 1,
    AUTH_HMAC_SHA1_96 = 2,
    AUTH_DES_MAC = 3,
    AUTH_KPDK_MD5 = 4,
    AUTH_AES_XCBC_96 = 5,
    AUTH_HMAC_MD5_128 = 6,
    AUTH_HMAC_SHA1_160 = 7,
    AUTH_AES_CMAC_96 = 8,
    AUTH_AES_128_GMAC = 9,
    AUTH_AES_192_GMAC = 10,
    AUTH_AES_256_GMAC = 11,
    AUTH_HMAC_SHA2_256_128 = 12,
    AUTH_HMAC_SHA2_384_192 = 13,
    AUTH_HMAC_SHA2_512_256 = 14,
}

#[derive(Copy, Clone, Debug, PartialEq, Eq, PartialOrd, Ord, FromPrimitive)]
#[repr(u16)]
pub(crate) enum DhTransform {
    MODP_768_bit = 1,
    MODP_1024_bit = 2,
    MODP_1536_bit = 5,
    MODP_2048_bit = 14,
    MODP_3072_bit = 15,
    MODP_4096_bit = 16,
    MODP_6144_bit = 17,
    MODP_8192_bit = 18,
}

#[derive(Copy, Clone, Debug, PartialEq, Eq, PartialOrd, Ord, FromPrimitive)]
#[repr(u8)]
pub(crate) enum EsnTransform {
    None = 0,
    Some = 1,
}

#[derive(Copy, Clone, Debug, PartialEq, Eq, PartialOrd, Ord, FromPrimitive)]
#[repr(u16)]
// NOTE(eta): there are extra constants in here from TS 24.302
pub(crate) enum NotificationType {
    UNSUPPORTED_CRITICAL_PAYLOAD = 1,
    INVALID_IKE_SPI = 4,
    INVALID_MAJOR_VERSION = 5,
    INVALID_SYNTAX = 7,
    INVALID_MESSAGE_ID = 9,
    INVALID_SPI = 11,
    NO_PROPOSAL_CHOSEN = 14,
    INVALID_KE_PAYLOAD = 17,
    AUTHENTICATION_FAILED = 24,
    SINGLE_PAIR_REQUIRED = 34,
    NO_ADDITIONAL_SAS = 35,
    INTERNAL_ADDRESS_FAILURE = 36,
    FAILED_CP_REQUIRED = 37,
    TS_UNACCEPTABLE = 38,
    INVALID_SELECTORS = 39,
    TEMPORARY_FAILURE = 43,
    CHILD_SA_NOT_FOUND = 44,
    PDN_CONNECTION_REJECTION = 8192,
    MAX_CONNECTION_REACHED = 8193,
    SEMANTIC_ERROR_IN_THE_TFT_OPERATION = 8241,
    SYNTACTICAL_ERROR_IN_THE_TFT_OPERATION = 8242,
    SEMANTIC_ERRORS_IN_PACKET_FILTERS = 8244,
    SYNTACTICAL_ERRORS_IN_PACKET_FILTERS = 8245,
    NON_3GPP_ACCESS_TO_EPC_NOT_ALLOWED = 9000,
    USER_UNKNOWN = 9001,
    NO_APN_SUBSCRIPTION = 9002,
    AUTHORIZATION_REJECTED = 9003,
    ILLEGAL_ME = 9006,
    NETWORK_FAILURE = 10500,
    RAT_TYPE_NOT_ALLOWED = 11001,
    IMEI_NOT_ACCEPTED = 11005,
    PLMN_NOT_ALLOWED = 11011,
    UNAUTHENTICATED_EMERGENCY_NOT_SUPPORTED = 11055,
    INITIAL_CONTACT = 16384,
    SET_WINDOW_SIZE = 16385,
    ADDITIONAL_TS_POSSIBLE = 16386,
    IPCOMP_SUPPORTED = 16387,
    NAT_DETECTION_SOURCE_IP = 16388,
    NAT_DETECTION_DESTINATION_IP = 16389,
    COOKIE = 16390,
    USE_TRANSPORT_MODE = 16391,
    HTTP_CERT_LOOKUP_SUPPORTED = 16392,
    REKEY_SA = 16393,
    ESP_TFC_PADDING_NOT_SUPPORTED = 16394,
    NON_FIRST_FRAGMENTS_ALSO = 16395,
    REACTIVATION_REQUESTED_CAUSE = 40961,
    BACKOFF_TIMER = 41041,
    PDN_TYPE_IPv4_ONLY_ALLOWED = 41050,
    PDN_TYPE_IPv6_ONLY_ALLOWED = 41051,
    DEVICE_IDENTITY = 41101,
    EMERGENCY_SUPPORT = 41112,
    EMERGENCY_CALL_NUMBERS = 41134,
    NBIFOM_GENERIC_CONTAINER = 41288,
    P_CSCF_RESELECTION_SUPPORT = 41304,
    PTI = 41501,
    IKEV2_MULTIPLE_BEARER_PDN_CONNECTIVITY = 42011,
    EPS_QOS = 42014,
    EXTENDED_EPS_QOS = 42015,
    TFT = 42017,
    MODIFIED_BEARER = 42020,
    APN_AMBR = 42094,
    EXTENDED_APN_AMBR = 42095,
    N1_MODE_CAPABILITY = 51015,
}
impl NotificationType {
    pub(crate) fn is_error(&self) -> bool {
        (*self as u16) < 16384
    }
}

#[derive(Copy, Clone, Debug, PartialEq, Eq, PartialOrd, Ord, FromPrimitive)]
#[repr(u8)]
pub enum ConfigurationType {
    Request = 1,
    Reply = 2,
    Set = 3,
    Ack = 4,
}

#[derive(Copy, Clone, Debug, PartialEq, Eq, PartialOrd, Ord, FromPrimitive)]
#[repr(u16)]
// NOTE(eta): there are extra constants in here from TS ??.???
pub enum ConfigurationAttrType {
    INTERNAL_IP4_ADDRESS = 1,
    INTERNAL_IP4_NETMASK = 2,
    INTERNAL_IP4_DNS = 3,
    INTERNAL_IP4_NBNS = 4,
    INTERNAL_IP4_DHCP = 6,
    APPLICATION_VERSION = 7,
    INTERNAL_IP6_ADDRESS = 8,
    INTERNAL_IP6_DNS = 10,
    INTERNAL_IP6_DHCP = 12,
    INTERNAL_IP4_SUBNET = 13,
    SUPPORTED_ATTRIBUTES = 14,
    INTERNAL_IP6_SUBNET = 15,
    MIP6_HOME_PREFIX = 16,
    INTERNAL_IP6_LINK = 17,
    INTERNAL_IP6_PREFIX = 18,
    HOME_AGENT_ADDRESS = 19,
    P_CSCF_IP4_ADDRESS = 20,
    P_CSCF_IP6_ADDRESS = 21,
    FTT_KAT = 22,
    EXTERNAL_SOURCE_IP4_NAT_INFO = 23,
    TIMEOUT_PERIOD_FOR_LIVENESS_CHECK = 24,
    INTERNAL_DNS_DOMAIN = 25,
    INTERNAL_DNSSEC_TA = 26,
}

#[derive(Copy, Clone, Debug, PartialEq, Eq, PartialOrd, Ord, FromPrimitive)]
#[repr(u8)]
pub enum IdentificationType {
    ID_IPV4_ADDR = 1,
    ID_FQDN = 2,
    ID_RFC822_ADDR = 3,
    ID_IPV6_ADDR = 5,
    ID_DER_ASN1_DN = 9,
    ID_DER_ASN1_GN = 10,
    ID_KEY_ID = 11,
}
