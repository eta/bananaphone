use crate::err::{InsideExt, ParseError, ParseResult};
use crate::ike::consts::{
    ConfigurationAttrType, ConfigurationType, DhTransform, IdentificationType, IkeExchangeType,
    IkePayloadType, IpsecProtocol, NotificationType,
};
use crate::ike::crypto::{encrypt_payloads, Sha1KeyingMaterial};
use crate::ike::sa::IkeSaProposal;
use anyhow::anyhow;
use hmac::{Hmac, Mac};
use log::{debug, info};
use num_traits::FromPrimitive;
use rand::RngCore;
use sha1::Sha1;
use std::net::{IpAddr, Ipv4Addr, SocketAddr, SocketAddrV4, SocketAddrV6};
use std::ops::Not;
use untrustended::ReaderExt;

pub(crate) mod consts;
pub(crate) mod crypto;
pub(crate) mod sa;
#[cfg(test)]
mod test;

#[derive(Clone, Debug, PartialEq)]
pub(crate) struct IkeMessage {
    /// A value chosen by the initiator to identify a unique IKE Security Association.
    ///
    /// This value MUST NOT be zero.
    initiator_spi: [u8; 8],
    /// A value chosen by the responder to identify a unique IKE Security Association.
    ///
    /// This value MUST NOT be zero.
    responder_spi: [u8; 8],
    /// Indicates the major version of the IKE
    /// protocol in use.
    ///
    /// Implementations based on this version of IKE
    /// MUST set the major version to 2.  Implementations based on
    /// previous versions of IKE and ISAKMP MUST set the major version to
    /// 1.  Implementations based on this version of IKE MUST reject or
    /// ignore messages containing a version number greater than 2 with an
    /// INVALID_MAJOR_VERSION notification message as described in Section
    /// 2.5.
    version_major: u8,
    /// Indicates the minor version of the IKE
    /// protocol in use.
    ///
    /// Implementations based on this version of IKE
    /// MUST set the minor version to 0.  They MUST ignore the minor
    /// version number of received messages.
    version_minor: u8,
    /// Indicates the type of exchange being
    /// used.  This constrains the payloads sent in each message in an
    /// exchange.
    exchange_type: IkeExchangeType,
    /// This bit indicates that this message is a
    /// response to a message containing the same Message ID.  This bit
    /// MUST be cleared in all request messages and MUST be set in all
    /// responses.  An IKE endpoint MUST NOT generate a response to a
    /// message that is marked as being a response (with one exception;
    /// see Section 2.21.2).
    flag_response: bool,
    /// This bit indicates that the transmitter is
    /// capable of speaking a higher major version number of the
    /// protocol than the one indicated in the major version number
    /// field.  Implementations of IKEv2 MUST clear this bit when
    /// sending and MUST ignore it in incoming messages.
    flag_version: bool,
    /// This bit MUST be set in messages sent by the
    /// original initiator of the IKE SA and MUST be cleared in
    /// messages sent by the original responder.
    ///
    /// It is used by the
    /// recipient to determine which eight octets of the SPI were
    /// generated by the recipient.  This bit changes to reflect who
    /// initiated the last rekey of the IKE SA.
    flag_initiator: bool,
    /// Unique packet message ID.
    message_id: u32,
    payloads: Vec<IkePayload>,
}

impl IkeMessage {
    fn parse_next_payload(next_payload: u8) -> ParseResult<Option<IkePayloadType>> {
        (next_payload != 0)
            .then(|| {
                IkePayloadType::from_u8(next_payload)
                    .ok_or_else(|| ParseError::invalid_for(next_payload, "next payload type"))
            })
            .transpose()
    }
    pub(crate) fn parse(buf: &[u8]) -> ParseResult<Self> {
        let mut buf = untrusted::Reader::new(untrusted::Input::from(buf));

        let initiator_spi = buf.read_bytes_less_safe(8)?.to_owned();
        let responder_spi = buf.read_bytes_less_safe(8)?.to_owned();
        let mut next_payload = Self::parse_next_payload(buf.read_byte()?).err_inside("header")?;
        let version = buf.read_byte()?;
        if version >> 4 != 2 {
            return Err(ParseError::VersionMismatch);
        }
        let exchange_type = buf.read_byte()?;
        let exchange_type = IkeExchangeType::from_u8(exchange_type)
            .ok_or_else(|| ParseError::invalid_for(exchange_type, "exchange type"))?;
        let flags = buf.read_byte()?;
        let flag_response = flags & 0b00100000 > 0;
        let flag_version = flags & 0b00010000 > 0;
        let flag_initiator = flags & 0b00001000 > 0;
        let message_id = buf.read_u32be()?;
        let length = buf.read_u32be()?;
        let mut payloads = vec![];
        while let Some(payload_ty) = next_payload {
            next_payload = Self::parse_next_payload(buf.read_byte()?)
                .err_inside(format!("payload after {payload_ty:?}"))?;
            buf.skip(1)?; // reserved/critical bit
            let payload_len = buf.read_u16be()?;

            payloads.push(match payload_ty {
                IkePayloadType::SecurityAssociation => {
                    let mut proposals = vec![];
                    let mut is_last = false;
                    let mut expected_proposal_num = 1;

                    while !is_last {
                        let (proposal, was_last) =
                            IkeSaProposal::parse(&mut buf, expected_proposal_num)
                                .err_inside("SecurityAssociation")?;
                        expected_proposal_num += 1;
                        is_last = was_last;
                        proposals.push(proposal);
                    }
                    IkePayload::SecurityAssociation(proposals)
                }
                IkePayloadType::KeyExchange => IkePayload::KeyExchange(
                    IkeKeyExchange::parse(&mut buf).err_inside("KeyExchange")?,
                ),
                IkePayloadType::Nonce => {
                    let nonce_len = payload_len - 4;
                    if nonce_len < 16 || nonce_len > 256 {
                        return Err(ParseError::invalid_for(nonce_len, "nonce payload length"));
                    }
                    let nonce = buf
                        .read_bytes_less_safe(nonce_len as usize)
                        .err_inside("nonce data")?
                        .to_owned();
                    IkePayload::Nonce { nonce }
                }
                IkePayloadType::Notify => IkePayload::Notify(
                    IkeNotify::parse(&mut buf, payload_len).err_inside("Notify")?,
                ),
                IkePayloadType::TrafficSelectorInitiator
                | IkePayloadType::TrafficSelectorResponder => {
                    let initiator = payload_ty == IkePayloadType::TrafficSelectorInitiator;
                    let ts_count = buf.read_byte()?;
                    buf.skip(3)?; // reserved bytes
                    let mut selectors = vec![];
                    for _ in 0..ts_count {
                        selectors.push(IkeTrafficSelector::parse(&mut buf)?);
                    }
                    IkePayload::TrafficSelector {
                        initiator,
                        selectors,
                    }
                }
                _ => {
                    eprintln!("skipping an unknown payload of type {payload_ty:?}");
                    buf.skip(payload_len as usize)?;
                    continue;
                }
            });
        }
        Ok(Self {
            initiator_spi: initiator_spi.try_into().unwrap(),
            responder_spi: responder_spi.try_into().unwrap(),
            version_major: version >> 4,
            version_minor: version & 0b00001111,
            exchange_type,
            flag_response,
            flag_version,
            flag_initiator,
            message_id,
            payloads,
        })
    }
    pub(crate) fn sa_init() -> Self {
        let mut initiator_spi = [0; 8];
        rand::thread_rng().fill_bytes(&mut initiator_spi);

        Self {
            initiator_spi,
            responder_spi: [0; 8],
            version_major: 2,
            version_minor: 0,
            exchange_type: IkeExchangeType::SaInit,
            flag_response: false,
            flag_version: false,
            flag_initiator: true,
            message_id: 0,
            payloads: vec![],
        }
    }

    pub(crate) fn respond_with_auth(&self) -> Self {
        Self {
            initiator_spi: self.initiator_spi,
            responder_spi: self.responder_spi,
            version_major: 2,
            version_minor: 0,
            exchange_type: IkeExchangeType::Auth,
            flag_response: false,
            flag_version: false,
            flag_initiator: true,
            message_id: self.message_id + 1,
            payloads: vec![],
        }
    }

    pub(crate) fn with_payload(mut self, payload: IkePayload) -> Self {
        self.payloads.push(payload);
        self
    }

    pub(crate) fn payloads(&self) -> &[IkePayload] {
        &self.payloads
    }

    pub(crate) fn payloads_mut(&mut self) -> &mut [IkePayload] {
        &mut self.payloads
    }

    pub(crate) fn initiator_spi(&self) -> &[u8; 8] {
        &self.initiator_spi
    }

    pub(crate) fn responder_spi(&self) -> &[u8; 8] {
        &self.responder_spi
    }

    pub(crate) fn security_association(&self) -> Option<&[IkeSaProposal]> {
        for payload in self.payloads.iter() {
            if let IkePayload::SecurityAssociation(ret) = payload {
                return Some(ret);
            }
        }
        None
    }

    pub(crate) fn key_exchange(&self) -> Option<&IkeKeyExchange> {
        for payload in self.payloads.iter() {
            if let IkePayload::KeyExchange(ret) = payload {
                return Some(ret);
            }
        }
        None
    }

    pub(crate) fn nonce(&self) -> Option<&[u8]> {
        for payload in self.payloads.iter() {
            if let IkePayload::Nonce { nonce } = payload {
                return Some(nonce);
            }
        }
        None
    }

    pub(crate) fn notifications(&self) -> Vec<&IkeNotify> {
        self.payloads
            .iter()
            .filter_map(|x| {
                if let IkePayload::Notify(x) = x {
                    Some(x)
                } else {
                    None
                }
            })
            .collect()
    }

    fn write_header(&self, ret: &mut Vec<u8>, next_payload: IkePayloadType) -> usize {
        // initiator's spi
        ret.extend_from_slice(&self.initiator_spi);
        // responder's spi
        ret.extend_from_slice(&self.responder_spi);
        // next payload
        ret.push(next_payload as u8);
        // major/minor version
        ret.push(self.version_major << 4 | self.version_minor);
        // exchange type
        ret.push(self.exchange_type as u8);
        // flags
        let mut flags = 0u8;
        if self.flag_initiator {
            flags |= 1 << 3;
        }
        if self.flag_version {
            flags |= 1 << 4;
        }
        if self.flag_response {
            flags |= 1 << 5;
        }
        ret.push(flags);
        // message id
        ret.extend_from_slice(&self.message_id.to_be_bytes());
        // length (u32, we will fixup later)
        let length_idx = ret.len();
        ret.extend_from_slice(&[0, 0, 0, 0]);
        length_idx
    }

    fn write_payloads(&self, ret: &mut Vec<u8>) {
        for (i, payload) in self.payloads.iter().enumerate() {
            let payload_start_idx = ret.len();

            let is_last = i + 1 == self.payloads.len();
            let next_payload = is_last
                .not()
                .then(|| self.payloads.get(i + 1).unwrap().payload_type() as u8)
                .unwrap_or(0);
            // next payload
            ret.push(next_payload);
            // critical/reserved bit
            ret.push(0);
            // payload length (u16, to be fixed up)
            let payload_length_idx = ret.len();
            ret.extend_from_slice(&[0, 0]);
            match payload {
                IkePayload::SecurityAssociation(sa) => {
                    for (i, proposal) in sa.iter().enumerate() {
                        let is_last = i + 1 == sa.len();
                        proposal.write(ret, is_last, (i + 1) as u8);
                    }
                }
                IkePayload::KeyExchange(ke) => {
                    ke.write(ret);
                }
                IkePayload::Nonce { nonce } => {
                    ret.extend_from_slice(nonce);
                }
                IkePayload::TrafficSelector { selectors, .. } => {
                    ret.push(selectors.len() as u8);
                    ret.extend_from_slice(&[0, 0, 0]);
                    for selector in selectors {
                        selector.write(ret);
                    }
                }
                IkePayload::Configuration(cp) => {
                    ret.push(cp.kind as u8);
                    ret.extend_from_slice(&[0, 0, 0]);
                    for attr in cp.attrs.iter() {
                        attr.write(ret);
                    }
                }
                IkePayload::Identification { data, .. } => {
                    data.write(ret);
                }
                IkePayload::Notify(_) => unimplemented!(),
            }
            let payload_length: u16 = ret.len() as u16 - payload_start_idx as u16;
            ret[payload_length_idx..(payload_length_idx + 2)]
                .copy_from_slice(&payload_length.to_be_bytes());
        }
    }

    pub(crate) fn as_bytes(&self) -> anyhow::Result<Vec<u8>> {
        let mut ret = vec![];
        // header
        let payload_type = self
            .payloads
            .first()
            .ok_or_else(|| anyhow!("ike message has no payloads"))?
            .payload_type();
        let length_idx = self.write_header(&mut ret, payload_type);
        // payloads
        self.write_payloads(&mut ret);

        let length: u32 = ret.len() as u32;
        ret[length_idx..(length_idx + 4)].copy_from_slice(&length.to_be_bytes());

        Ok(ret)
    }

    pub(crate) fn as_bytes_encrypted(
        &self,
        keying_material: &Sha1KeyingMaterial,
    ) -> anyhow::Result<Vec<u8>> {
        let mut ret = vec![];
        // header
        let length_idx = self.write_header(&mut ret, IkePayloadType::EncryptedAuthenticated);

        // plaintext payloads
        let mut payloads = vec![];
        let first_payload = self
            .payloads
            .first()
            .ok_or_else(|| anyhow!("ike message has no payloads"))?
            .payload_type();
        self.write_payloads(&mut payloads);

        // ..which we encrypt to get the encrypted payload data
        let (iv, encrypted_payload) = encrypt_payloads(&payloads, keying_material);
        info!("initiator SPI: {}", hex::encode(&self.initiator_spi));
        info!("responder SPI: {}", hex::encode(&self.responder_spi));
        info!("SK_ei: {}", hex::encode(&keying_material.sk_ei));
        info!("SK_er: {}", hex::encode(&keying_material.sk_er));
        info!("SK_ai: {}", hex::encode(&keying_material.sk_ai));
        info!("SK_ar: {}", hex::encode(&keying_material.sk_ar));

        // encrypted payload header: next payload (actually first payload)
        ret.push(first_payload as u8);
        // critical/reserved bit
        ret.push(0);
        // payload length
        let encrypted_payload_len = encrypted_payload.len() as u16 + 12 + iv.len() as u16 + 4;
        ret.extend_from_slice(&encrypted_payload_len.to_be_bytes());
        // initialization vector
        ret.extend_from_slice(&iv);
        // data
        ret.extend_from_slice(&encrypted_payload);

        // we now need to write the length on properly so we can integrity checksum the whole thing
        // AUTH_HMAC_SHA1_96 has a 96-bit output => 12 bytes
        let length: u32 = ret.len() as u32 + 12;
        ret[length_idx..(length_idx + 4)].copy_from_slice(&length.to_be_bytes());

        // now we do the HMAC:
        let mut hmac: Hmac<Sha1> = Hmac::new_from_slice(&keying_material.sk_ai).unwrap();
        hmac.update(&ret);
        let output = hmac.finalize().into_bytes().to_vec();
        ret.extend_from_slice(&output[0..12]);

        Ok(ret)
    }
}

#[derive(Clone, Debug, PartialEq)]
pub(crate) enum IkePayload {
    SecurityAssociation(Vec<IkeSaProposal>),
    KeyExchange(IkeKeyExchange),
    Nonce {
        nonce: Vec<u8>,
    },
    Notify(IkeNotify),
    TrafficSelector {
        initiator: bool,
        selectors: Vec<IkeTrafficSelector>,
    },
    Configuration(IkeConfiguration),
    Identification {
        initiator: bool,
        data: IkeIdentificationData,
    },
}

impl IkePayload {
    pub(crate) fn new_sa(proposals: impl IntoIterator<Item = IkeSaProposal>) -> Self {
        Self::SecurityAssociation(proposals.into_iter().collect())
    }

    pub(crate) fn new_ke(group: DhTransform, dh_public: Vec<u8>) -> Self {
        Self::KeyExchange(IkeKeyExchange { group, dh_public })
    }

    pub(crate) fn new_tsi(selectors: impl IntoIterator<Item = IkeTrafficSelector>) -> Self {
        Self::TrafficSelector {
            initiator: true,
            selectors: selectors.into_iter().collect(),
        }
    }

    pub(crate) fn new_tsr(selectors: impl IntoIterator<Item = IkeTrafficSelector>) -> Self {
        Self::TrafficSelector {
            initiator: false,
            selectors: selectors.into_iter().collect(),
        }
    }

    pub(crate) fn new_cp(data: IkeConfiguration) -> Self {
        Self::Configuration(data)
    }

    pub(crate) fn new_idi(data: IkeIdentificationData) -> Self {
        Self::Identification {
            initiator: true,
            data,
        }
    }

    pub(crate) fn new_idr(data: IkeIdentificationData) -> Self {
        Self::Identification {
            initiator: false,
            data,
        }
    }

    pub(crate) fn new_nonce(nonce: Vec<u8>) -> Self {
        Self::Nonce { nonce }
    }

    pub(crate) fn payload_type(&self) -> IkePayloadType {
        match self {
            IkePayload::SecurityAssociation(..) => IkePayloadType::SecurityAssociation,
            IkePayload::KeyExchange(..) => IkePayloadType::KeyExchange,
            IkePayload::Nonce { .. } => IkePayloadType::Nonce,
            IkePayload::Notify(..) => IkePayloadType::Notify,
            IkePayload::TrafficSelector { initiator, .. } => {
                if *initiator {
                    IkePayloadType::TrafficSelectorInitiator
                } else {
                    IkePayloadType::TrafficSelectorResponder
                }
            }
            IkePayload::Configuration(..) => IkePayloadType::Configuration,
            IkePayload::Identification { initiator, .. } => {
                if *initiator {
                    IkePayloadType::IdentificationInitiator
                } else {
                    IkePayloadType::IdentificationResponder
                }
            }
        }
    }
}

#[derive(Clone, Debug, PartialEq)]
pub(crate) struct IkeKeyExchange {
    pub(crate) group: DhTransform,
    pub(crate) dh_public: Vec<u8>,
}

impl IkeKeyExchange {
    fn parse(reader: &mut untrusted::Reader) -> ParseResult<Self> {
        let group_num = reader.read_u16be()?;
        let group = DhTransform::from_u16(group_num)
            .ok_or_else(|| ParseError::invalid_for(group_num, "DH group num inside KE"))?;
        reader.skip(2)?; // reserved
        let length: usize = match group {
            DhTransform::MODP_768_bit => 768 / 8,
            DhTransform::MODP_1024_bit => 1024 / 8,
            DhTransform::MODP_1536_bit => 1536 / 8,
            DhTransform::MODP_2048_bit => 2048 / 8,
            DhTransform::MODP_3072_bit => 3072 / 8,
            DhTransform::MODP_4096_bit => 4096 / 8,
            DhTransform::MODP_6144_bit => 6144 / 8,
            DhTransform::MODP_8192_bit => 8192 / 8,
        };
        let dh_public = reader.read_bytes_less_safe(length)?.to_owned();
        Ok(Self { group, dh_public })
    }
    fn write(&self, out: &mut Vec<u8>) {
        // group number
        out.extend_from_slice(&(self.group as u16).to_be_bytes());
        // reserved fields
        out.push(0);
        out.push(0);
        // dh data
        out.extend_from_slice(&self.dh_public);
    }
}

#[derive(Clone, Debug, PartialEq)]
pub(crate) struct IkeNotify {
    protocol: Option<IpsecProtocol>,
    spi: Vec<u8>,
    pub(crate) data: IkeNotifyData,
}

#[derive(Clone, Debug, PartialEq)]
pub(crate) enum IkeNotifyData {
    InvalidKePayload { accepted_group: DhTransform },
    Unimplemented { ty: NotificationType, data: Vec<u8> },
    Unknown { ty: u16, data: Vec<u8> },
}

impl IkeNotifyData {
    pub(crate) fn is_error(&self) -> bool {
        match self {
            IkeNotifyData::InvalidKePayload { .. } => true,
            IkeNotifyData::Unimplemented { ty, .. } => (*ty as u16) < 16384,
            IkeNotifyData::Unknown { ty, .. } => *ty < 16384,
        }
    }
}

impl IkeNotify {
    pub(crate) fn parse(
        reader: &mut untrusted::Reader<'_>,
        payload_length: u16,
    ) -> ParseResult<Self> {
        let protocol_id = reader.read_byte()?;
        let protocol = (protocol_id != 0)
            .then(|| {
                IpsecProtocol::from_u8(protocol_id)
                    .ok_or_else(|| ParseError::invalid_for(protocol_id, "ike notify protocol id"))
            })
            .transpose()?;
        let spi_size = reader.read_byte()?;
        let message_type = reader.read_u16be()?;
        let spi = reader.read_bytes_less_safe(spi_size as usize)?.to_owned();
        // need to be careful that a malformed packet can't cause us to underflow here!
        let remainder = (payload_length as usize).saturating_sub((spi_size + 8) as usize);
        let data = match NotificationType::from_u16(message_type) {
            Some(NotificationType::INVALID_KE_PAYLOAD) => {
                let group_id = reader.read_u16be()?;
                let group = DhTransform::from_u16(group_id).ok_or_else(|| {
                    ParseError::invalid_for(group_id, "invalid ke payload group id")
                })?;
                IkeNotifyData::InvalidKePayload {
                    accepted_group: group,
                }
            }
            Some(unknown) => {
                let data = reader.read_bytes_less_safe(remainder)?.to_owned();
                IkeNotifyData::Unimplemented { ty: unknown, data }
            }
            None => {
                let data = reader.read_bytes_less_safe(remainder)?.to_owned();
                IkeNotifyData::Unknown {
                    ty: message_type,
                    data,
                }
            }
        };
        Ok(Self {
            protocol,
            spi,
            data,
        })
    }
}

// NOTE(eta): This is a *terrible* way to represent the start and end ports. Oh well.
#[derive(Clone, Debug, PartialEq)]
pub(crate) enum IkeTrafficSelector {
    Ipv4 {
        protocol: u8,
        start: SocketAddrV4,
        end: SocketAddrV4,
    },
    Ipv6 {
        protocol: u8,
        start: SocketAddrV6,
        end: SocketAddrV6,
    },
}
impl IkeTrafficSelector {
    pub(crate) fn parse(reader: &mut untrusted::Reader<'_>) -> ParseResult<Self> {
        let ts_type = reader.read_byte()?;
        let protocol = reader.read_byte()?;
        let _selector_length: u16 = reader.read_u16be()?;
        let start_port: u16 = reader.read_u16be()?;
        let end_port: u16 = reader.read_u16be()?;
        match ts_type {
            7 => {
                let start_address = reader.read_ipv4addr()?;
                let end_address = reader.read_ipv4addr()?;
                Ok(Self::Ipv4 {
                    protocol,
                    start: SocketAddrV4::new(start_address, start_port),
                    end: SocketAddrV4::new(end_address, end_port),
                })
            }
            8 => {
                let start_address = reader.read_ipv6addr()?;
                let end_address = reader.read_ipv6addr()?;
                Ok(Self::Ipv6 {
                    protocol,
                    start: SocketAddrV6::new(start_address, start_port, 0, 0),
                    end: SocketAddrV6::new(end_address, end_port, 0, 0),
                })
            }
            _ => Err(ParseError::invalid_for(ts_type, "traffic selector type")),
        }
    }
    fn write(&self, out: &mut Vec<u8>) {
        let (ty, protocol, start, end) = match self {
            IkeTrafficSelector::Ipv4 {
                protocol,
                start,
                end,
            } => (7u8, protocol, SocketAddr::V4(*start), SocketAddr::V4(*end)),
            IkeTrafficSelector::Ipv6 {
                protocol,
                start,
                end,
            } => (8, protocol, SocketAddr::V6(*start), SocketAddr::V6(*end)),
        };
        // TS type
        out.push(ty);
        // IP protocol ID
        out.push(*protocol);
        // length
        let length = 8u16
            + match self {
                IkeTrafficSelector::Ipv4 { .. } => 4 * 2,
                IkeTrafficSelector::Ipv6 { .. } => 16 * 2,
            };
        out.extend_from_slice(&length.to_be_bytes());
        // start port
        out.extend_from_slice(&start.port().to_be_bytes());
        // end port
        out.extend_from_slice(&end.port().to_be_bytes());
        // starting address
        match start.ip() {
            IpAddr::V4(i4) => out.extend_from_slice(&i4.octets()),
            IpAddr::V6(i6) => out.extend_from_slice(&i6.octets()),
        }
        // ending address
        match end.ip() {
            IpAddr::V4(i4) => out.extend_from_slice(&i4.octets()),
            IpAddr::V6(i6) => out.extend_from_slice(&i6.octets()),
        }
    }
}

#[derive(Clone, Debug, PartialEq)]
pub struct IkeConfiguration {
    pub(crate) kind: ConfigurationType,
    pub(crate) attrs: Vec<IkeConfigurationAttr>,
}

impl IkeConfiguration {
    pub fn request(attrs: impl IntoIterator<Item = ConfigurationAttrType>) -> Self {
        Self {
            kind: ConfigurationType::Request,
            attrs: attrs
                .into_iter()
                .map(IkeConfigurationAttr::TypeOnly)
                .collect(),
        }
    }
}

#[derive(Clone, Debug, PartialEq)]
pub enum IkeConfigurationAttr {
    TypeOnly(ConfigurationAttrType),
    TypeValue {
        ty: ConfigurationAttrType,
        data: Vec<u8>,
    },
    Unknown {
        ty: u16,
        data: Vec<u8>,
    },
}
impl IkeConfigurationAttr {
    fn parse(reader: &mut untrusted::Reader<'_>) -> ParseResult<Self> {
        // attribute type, with reserved bit cleared
        let ty = reader.read_u16be()? & !(1 << 15);
        // length
        let len = reader.read_u16be()?;
        // data
        let data = reader.read_bytes_less_safe(len as usize)?;

        Ok(match ConfigurationAttrType::from_u16(ty) {
            None => Self::Unknown {
                ty,
                data: data.to_vec(),
            },
            Some(t) if data.is_empty() => Self::TypeOnly(t),
            Some(t) => Self::TypeValue {
                ty: t,
                data: data.to_vec(),
            },
        })
    }
    fn write(&self, out: &mut Vec<u8>) {
        let (ty, len) = match self {
            IkeConfigurationAttr::TypeOnly(ty) => (*ty as u16, 0u16),
            IkeConfigurationAttr::TypeValue { ty, data } => (*ty as u16, data.len() as u16),
            IkeConfigurationAttr::Unknown { ty, data } => (*ty, data.len() as u16),
        };
        // type
        out.extend_from_slice(&ty.to_be_bytes());
        // length
        out.extend_from_slice(&len.to_be_bytes());
        // value
        match self {
            IkeConfigurationAttr::TypeValue { data, .. } => out.extend_from_slice(&data),
            IkeConfigurationAttr::Unknown { data, .. } => out.extend_from_slice(&data),
            _ => {}
        }
    }
}

#[derive(Clone, Debug, PartialEq)]
pub enum IkeIdentificationData {
    Rfc822Addr(String),
    Fqdn(String),
    Unimplemented {
        ty: IdentificationType,
        data: Vec<u8>,
    },
    Unknown {
        ty: u8,
        data: Vec<u8>,
    },
}
impl IkeIdentificationData {
    fn write(&self, out: &mut Vec<u8>) {
        match self {
            IkeIdentificationData::Rfc822Addr(addr) => {
                out.extend_from_slice(&[IdentificationType::ID_RFC822_ADDR as u8, 0, 0, 0]);
                out.extend_from_slice(addr.as_bytes());
            }
            IkeIdentificationData::Fqdn(addr) => {
                out.extend_from_slice(&[IdentificationType::ID_FQDN as u8, 0, 0, 0]);
                out.extend_from_slice(addr.as_bytes());
            }
            IkeIdentificationData::Unimplemented { ty, data } => {
                out.extend_from_slice(&[*ty as u8, 0, 0, 0]);
                out.extend_from_slice(data);
            }
            IkeIdentificationData::Unknown { ty, data } => {
                out.extend_from_slice(&[*ty, 0, 0, 0]);
                out.extend_from_slice(data);
            }
        }
    }
}
