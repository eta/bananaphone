use crate::err::{ParseError, ParseResult};
use crate::ike::consts::{
    DhTransform, EncrTransform, EsnTransform, IkeTransformType, IntegTransform, IpsecProtocol,
    PrfTransform,
};
use num_traits::FromPrimitive;
use untrustended::ReaderExt;

#[derive(Clone, Debug, PartialEq)]
pub(crate) struct IkeSaProposal {
    pub(super) protocol: IpsecProtocol,
    pub(super) spi: Vec<u8>,
    pub(super) transforms: Vec<IkeSaTransform>,
}

impl IkeSaProposal {
    pub fn parse(
        reader: &mut untrusted::Reader<'_>,
        expected_proposal_num: u8,
    ) -> ParseResult<(Self, bool)> {
        let is_last = reader.read_byte()? == 0;
        reader.skip(1)?; // reserved byte
        let _proposal_len = reader.read_u16be()?;
        let proposal_num = reader.read_byte()?;
        if proposal_num != expected_proposal_num {
            return Err(ParseError::ProposalNumMismatch {
                expected: expected_proposal_num,
                provided: proposal_num,
            });
        }
        let protocol_id = reader.read_byte()?;
        let protocol = IpsecProtocol::from_u8(protocol_id)
            .ok_or_else(|| ParseError::invalid_for(protocol_id, "sa proposal protocol id"))?;
        let spi_size = reader.read_byte()?;
        let num_transforms = reader.read_byte()?;
        let spi = reader.read_bytes_less_safe(spi_size as usize)?.to_owned();
        let mut transforms = vec![];

        for transform_i in 0..num_transforms {
            let (transform, is_last) = IkeSaTransform::parse(reader)?;
            if is_last != (transform_i + 1 == num_transforms) {
                return Err(ParseError::LengthMismatch("sa proposal transform count"));
            }
            transforms.push(transform);
        }

        Ok((
            Self {
                protocol,
                spi,
                transforms,
            },
            is_last,
        ))
    }
    pub fn new(proto: IpsecProtocol) -> Self {
        Self {
            protocol: proto,
            spi: vec![],
            transforms: vec![],
        }
    }

    pub fn with_spi(mut self, spi: Vec<u8>) -> Self {
        self.spi = spi;
        self
    }

    pub fn with_encr(mut self, encr: EncrTransform, key_length: Option<u16>) -> Self {
        self.transforms.push(IkeSaTransform::Encr {
            ty: encr,
            key_length,
        });
        self
    }

    pub fn with_prf(mut self, prf: PrfTransform) -> Self {
        self.transforms.push(IkeSaTransform::Prf(prf));
        self
    }

    pub fn with_integ(mut self, integ: IntegTransform) -> Self {
        self.transforms.push(IkeSaTransform::Integ(integ));
        self
    }

    pub fn with_dh(mut self, dh: DhTransform) -> Self {
        self.transforms.push(IkeSaTransform::Dh(dh));
        self
    }

    pub fn with_esn(mut self, esn: EsnTransform) -> Self {
        self.transforms.push(IkeSaTransform::Esn(esn));
        self
    }
}

impl IkeSaProposal {
    pub(crate) fn write(&self, out: &mut Vec<u8>, is_last: bool, idx: u8) {
        let start_idx = out.len();

        // is last
        out.push(if is_last { 0 } else { 2 });
        // reserved
        out.push(0);
        // proposal length (u16) -- store the index to fill later
        let length_idx = out.len();
        out.extend_from_slice(&[0, 0]);
        // proposal index
        out.push(idx);
        // protocol identifier
        out.push(self.protocol as u8);
        // spi size
        out.push(self.spi.len() as u8);
        // transform count
        out.push(self.transforms.len() as u8);
        // spi
        if !self.spi.is_empty() {
            out.extend_from_slice(&self.spi);
        }
        // transforms
        for (i, transform) in self.transforms.iter().enumerate() {
            let is_last = i + 1 == self.transforms.len();
            transform.write(out, is_last);
        }
        // set the length we didn't set earlier
        let proposal_length = (out.len() - start_idx) as u16;
        out[length_idx..(length_idx + 2)].copy_from_slice(&proposal_length.to_be_bytes());
    }
}

#[derive(Clone, Debug, PartialEq)]
pub(crate) enum IkeSaTransform {
    Encr {
        ty: EncrTransform,
        key_length: Option<u16>,
    },
    Prf(PrfTransform),
    Integ(IntegTransform),
    Dh(DhTransform),
    Esn(EsnTransform),
}

impl IkeSaTransform {
    fn parse(reader: &mut untrusted::Reader<'_>) -> ParseResult<(Self, bool)> {
        let is_last = reader.read_byte()? == 0;
        reader.skip(1)?; // reserved byte
        let transform_len = reader.read_u16be()?;
        let transform_type = reader.read_byte()?;
        reader.skip(1)?; // reserved byte
        let transform_type = IkeTransformType::from_u8(transform_type)
            .ok_or_else(|| ParseError::invalid_for(transform_type, "sa transform type"))?;
        let transform_id = reader.read_u16be()?;
        // NOTE(eta): This isn't strictly conformant, but what do I care.
        let key_length = match transform_len {
            8 => None, // no attrs
            12 => {
                // has the key length attribute
                let attr_key = reader.read_u16be()?;
                if attr_key != (1 << 15) | 14 {
                    return Err(ParseError::invalid_for(
                        attr_key,
                        "sa transform attribute key",
                    ));
                }
                let key_len = reader.read_u16be()?;
                Some(key_len)
            }
            // something we weren't expecting
            _ => {
                return Err(ParseError::invalid_for(
                    transform_len,
                    "sa transform length",
                ))
            }
        };
        let ret = match transform_type {
            IkeTransformType::Encr => Self::Encr {
                ty: EncrTransform::from_u16(transform_id)
                    .ok_or_else(|| ParseError::invalid_for(transform_id, "encr transform id"))?,
                key_length,
            },
            IkeTransformType::Prf => Self::Prf(
                PrfTransform::from_u16(transform_id)
                    .ok_or_else(|| ParseError::invalid_for(transform_id, "prf transform id"))?,
            ),
            IkeTransformType::Integ => Self::Integ(
                IntegTransform::from_u16(transform_id)
                    .ok_or_else(|| ParseError::invalid_for(transform_id, "integ transform id"))?,
            ),
            IkeTransformType::Dh => Self::Dh(
                DhTransform::from_u16(transform_id)
                    .ok_or_else(|| ParseError::invalid_for(transform_id, "dh transform id"))?,
            ),
            IkeTransformType::Esn => Self::Esn(
                EsnTransform::from_u16(transform_id)
                    .ok_or_else(|| ParseError::invalid_for(transform_id, "esn transform id"))?,
            ),
        };
        Ok((ret, is_last))
    }
    fn write(&self, out: &mut Vec<u8>, is_last: bool) {
        use self::IkeSaTransform::*;

        let (transform_type, transform_id) = match self {
            Encr { ty, .. } => (0x1_u8, *ty as u16),
            Prf(p) => (0x2, *p as u16),
            Integ(p) => (0x3, *p as u16),
            Dh(p) => (0x4, *p as u16),
            Esn(p) => (0x5, *p as u16),
        };

        let attribute: Option<u32> = if let Encr {
            key_length: Some(len),
            ..
        } = self
        {
            let value: u32 = 1 << 31 | 14 << 16 | (*len as u32);
            Some(value)
        } else {
            None
        };

        // is last
        out.push(if is_last { 0 } else { 3 });
        // reserved field
        out.push(0);
        // transform length (u16)
        let length: u16 = 8 + if attribute.is_some() { 4 } else { 0 };
        out.extend_from_slice(&length.to_be_bytes());
        // transform type
        out.push(transform_type);
        // reserved field
        out.push(0);
        // transform id
        out.extend_from_slice(&transform_id.to_be_bytes());
        // transform attributes
        if let Some(a) = attribute {
            out.extend_from_slice(&a.to_be_bytes());
        }
    }
}
