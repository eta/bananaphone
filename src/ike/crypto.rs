//! Cryptography primitives for IKEv2.

use crate::dh::DiffieKey2048;
use crate::ike::consts::IkePayloadType;
use aes::cipher::block_padding::Pkcs7;
use aes::cipher::BlockEncryptMut;
use cbc::cipher::KeyIvInit;
use hmac::{Hmac, Mac};
use log::info;
use rand::RngCore;
use sha1::Sha1;

/// A holder for all of the IKEv2 SA Keying Material, generic over different crypto algorithms
/// used.
///
/// `PRF_LEN` is the output size for the pseudorandom function; `ENC_LEN` is the key size for
/// the encryption function; and `INTEG_LEN` is the key size for the integrity function.
#[derive(Debug, Clone, PartialEq)] // NOTE(eta): the Debug impl is spicy!
pub(crate) struct KeyingMaterial<const PRF_LEN: usize, const ENC_LEN: usize, const INTEG_LEN: usize>
{
    /// Key for deriving new keys for Child SAs
    pub(crate) sk_d: [u8; PRF_LEN],
    /// Key for the integrity protection algorithm (initiator)
    pub(crate) sk_ai: [u8; INTEG_LEN],
    /// Key for the integrity protection algorithm (responder)
    pub(crate) sk_ar: [u8; INTEG_LEN],
    /// Key for the encryption algorithm (initiator)
    pub(crate) sk_ei: [u8; ENC_LEN],
    /// Key for the encryption algorithm (responder)
    pub(crate) sk_er: [u8; ENC_LEN],
    /// Key for generating an AUTH payload (initiator)
    pub(crate) sk_pi: [u8; PRF_LEN],
    /// Key for generating an AUTH payload (responder)
    pub(crate) sk_pr: [u8; PRF_LEN],
}

/// A specific instantiation of `KeyingMaterial` for a handshake using SHA1 functions
/// (`PRF_HMAC_SHA1`, `AUTH_HMAC_SHA1_96`) and 128-bit encryption (`ENCR_AES_CBC`).
pub(crate) type Sha1KeyingMaterial = KeyingMaterial<20, 16, 20>;

fn sha1_prf_plus(key: &[u8], data: &[u8], desired_length: usize) -> Vec<u8> {
    let mut ret = vec![];
    let mut last = vec![];
    let mut index = 0x01;
    while ret.len() < desired_length {
        let mut hmac: Hmac<Sha1> = Hmac::new_from_slice(&key).unwrap();
        let mut iteration_data = vec![];
        iteration_data.extend_from_slice(&last);
        iteration_data.extend_from_slice(data);
        iteration_data.push(index);
        hmac.update(&iteration_data);
        let output = hmac.finalize().into_bytes().to_vec();
        ret.extend_from_slice(&output);
        last = output;
        index += 1;
    }
    ret
}

impl Sha1KeyingMaterial {
    /// Generate keying material according to the spec from a set of nonces, a DH private key,
    /// a set of SPIs, and the peer's DH response.
    ///
    /// FIXME(eta): This assumes 2048-bit DH for now.
    pub(crate) fn generate(
        nonce_i: &[u8],
        nonce_r: &[u8],
        spi_i: &[u8],
        spi_r: &[u8],
        dh_key: &DiffieKey2048,
        dh_response: [u8; 256],
    ) -> Self {
        // SKEYSEED = prf(Ni | Nr, g^ir)
        let mut hmac_key = vec![];
        hmac_key.extend_from_slice(nonce_i);
        hmac_key.extend_from_slice(nonce_r);

        let dh_secret = dh_key.derive_secret(dh_response);
        info!("dh secret: {}", hex::encode(&dh_secret));

        let mut hmac: Hmac<Sha1> = Hmac::new_from_slice(&hmac_key).unwrap();
        hmac.update(&dh_secret);

        let skeyseed = hmac.finalize().into_bytes();

        // {SK_d | SK_ai | SK_ar | SK_ei | SK_er | SK_pi | SK_pr}
        //                = prf+ (SKEYSEED, Ni | Nr | SPIi | SPIr)

        // generate the Ni | Nr | SPIi | SPIr bit
        let mut prf_plus_data = vec![];
        prf_plus_data.extend_from_slice(nonce_i);
        prf_plus_data.extend_from_slice(nonce_r);
        prf_plus_data.extend_from_slice(spi_i);
        prf_plus_data.extend_from_slice(spi_r);

        // PRF_LEN * 3 + ENC_LEN * 2 + INTEG_LEN * 2
        let required_data_length = 20 * 3 + 16 * 2 + 20 * 2;
        // generate a long string of octets to get the SK_* from
        let material = sha1_prf_plus(&skeyseed, &prf_plus_data, required_data_length);

        Self {
            sk_d: material[0..20].try_into().unwrap(),
            sk_ai: material[20..40].try_into().unwrap(),
            sk_ar: material[40..60].try_into().unwrap(),
            sk_ei: material[60..76].try_into().unwrap(),
            sk_er: material[76..92].try_into().unwrap(),
            sk_pi: material[92..112].try_into().unwrap(),
            sk_pr: material[112..132].try_into().unwrap(),
        }
    }
}

type Aes128CbcEnc = cbc::Encryptor<aes::Aes128>;

pub(crate) fn encrypt_payloads(
    data: &[u8],
    keying_material: &Sha1KeyingMaterial,
) -> ([u8; 16], Vec<u8>) {
    let mut iv = [0; 16];
    rand::thread_rng().fill_bytes(&mut iv);
    let key = keying_material.sk_ei;
    let enc = Aes128CbcEnc::new(&key.into(), &iv.into());
    (iv, enc.encrypt_padded_vec_mut::<Pkcs7>(data))
}

#[cfg(test)]
mod test {
    use crate::ike::crypto::sha1_prf_plus;

    #[test]
    fn python_prf_plus() {
        let skeyseed = hex::decode("c1f4b9c5f0ffb3ea8e6b25e7f6ccb5c5263a5521").unwrap();
        let stream = hex::decode("39845ef05adf98116dfeab8228f6976d28fc04ca99b81919670c7494d79699c500ebb972d4deeb4bd55d747ac19b6e50").unwrap();
        let required_data_length = 20 * 3 + 16 * 2 + 20 * 2;
        let material = sha1_prf_plus(&skeyseed, &stream, required_data_length);
        assert_eq!(
            material[0..20],
            hex::decode("c369b71ed8de051ed0ae4fca05ae6af80afd8397").unwrap()
        );
        assert_eq!(
            material[20..40],
            hex::decode("41e6f0bd47dc55b3e052fd61e9c49800151ba593").unwrap()
        );
        assert_eq!(
            material[40..60],
            hex::decode("c6427fda888929f833d3c6b7c53a46093952a9ac").unwrap()
        );
        assert_eq!(
            material[60..76],
            hex::decode("1d7fc1a1d44d45c1d9e992ed4d414e0f").unwrap()
        );
        assert_eq!(
            material[76..92],
            hex::decode("f89e88f4dc1cbe906fd39b55b97769f1").unwrap()
        );
        assert_eq!(
            material[92..112],
            hex::decode("34e3a5572316d90738b216e294e008392c46ee87").unwrap()
        );
        assert_eq!(
            material[112..132],
            hex::decode("41f84b52fce24bb5c3b0fdc27cbfebb05098557e").unwrap()
        );
    }
}
