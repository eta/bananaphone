use crate::dh::DiffieKey2048;
use crate::ike::consts::{
    ConfigurationAttrType, DhTransform, EncrTransform, EsnTransform, IntegTransform, IpsecProtocol,
    PrfTransform,
};
use crate::ike::crypto::Sha1KeyingMaterial;
use crate::ike::sa::IkeSaProposal;
use crate::ike::{
    IkeConfiguration, IkeIdentificationData, IkeMessage, IkeNotify, IkeNotifyData, IkePayload,
    IkeTrafficSelector,
};
use anyhow::{anyhow, bail, Context};
use hmac::{Hmac, Mac};
use log::{debug, info, LevelFilter};
use rand::{Rng, RngCore};
use sha1::Sha1;
use std::net::{Ipv4Addr, ToSocketAddrs, UdpSocket};

mod dh;
mod err;
mod ike;

struct Handshake {
    socket: UdpSocket,
    imsi: String,
}

impl Handshake {
    fn new(mcc: u16, mnc: u16, imsi: String) -> anyhow::Result<Self> {
        if mcc > 999 || mnc > 999 {
            bail!("invalid mcc/mnc {mcc}/{mnc}");
        }
        let epdg_host = format!("epdg.epc.mnc{:03}.mcc{:03}.pub.3gppnetwork.org", mnc, mcc);
        debug!("resolving {epdg_host}");
        let addr = (epdg_host, 4500)
            .to_socket_addrs()
            .context("failed to resolve ePDG host")?
            .next()
            .ok_or_else(|| anyhow!("no addresses available ePDG host"))?;
        debug!("resolved as {}", addr);

        let socket =
            UdpSocket::bind((Ipv4Addr::UNSPECIFIED, 0)).context("failed to bind UDP socket")?;
        socket
            .connect(addr)
            .context("failed to connect UDP socket")?;

        Ok(Self { socket, imsi })
    }

    fn send_ike_message(&mut self, ike: &IkeMessage) -> anyhow::Result<()> {
        let mut message = ike.as_bytes()?;
        self.send_bytes(message)
    }

    fn send_bytes(&mut self, mut message: Vec<u8>) -> anyhow::Result<()> {
        // Insert the UDP encapsulation padding bytes.
        for _ in 0..4 {
            message.insert(0, 0);
        }
        self.socket.send(&message)?;
        Ok(())
    }

    fn receive_ike_message(&mut self) -> anyhow::Result<IkeMessage> {
        let mut buf = [0u8; 3000];
        self.socket.recv(&mut buf)?;
        if !buf.starts_with(&[0, 0, 0, 0]) {
            bail!("message was not an IKE message");
        }
        Ok(IkeMessage::parse(&buf[4..])?)
    }
}

fn main() -> anyhow::Result<()> {
    pretty_env_logger::formatted_builder()
        .filter_level(LevelFilter::Debug)
        .parse_default_env()
        .init();

    info!("constructing handshake");
    // Rough outline:
    // - Send IKE_SA_INIT, receive one
    // - Use this to set up (some) encryption keys? for the session ("initial SA")?
    // [ at this point the payloads become encrypted according to the initial SA ]
    // - Send IKE_AUTH with NAI and APN
    // - Receive IKE_AUTH with the RAND/AUTN parameters that the SIM wants
    // - Do the SIM handshake and give the results to the server
    // - ...more stuff I guess
    let mut handshake = Handshake::new(234, 020, "".into())?;

    let dh_key = DiffieKey2048::new();
    let mut nonce = vec![0u8; 256];
    rand::thread_rng().fill_bytes(&mut nonce);

    info!("attempting handshake");
    info!("DH public key: {}", hex::encode(&dh_key.public_key()));

    // HDR, SAi1, KE -->
    let message = IkeMessage::sa_init()
        .with_payload(IkePayload::new_sa(vec![IkeSaProposal::new(
            IpsecProtocol::Ike,
        )
        .with_encr(EncrTransform::ENCR_AES_CBC, Some(128))
        .with_prf(PrfTransform::PRF_HMAC_SHA1)
        .with_integ(IntegTransform::AUTH_HMAC_SHA1_96)
        .with_dh(DhTransform::MODP_2048_bit)]))
        .with_payload(IkePayload::new_ke(
            DhTransform::MODP_2048_bit,
            dh_key.public_key().into(),
        ))
        .with_payload(IkePayload::new_nonce(nonce.clone()));

    handshake.send_ike_message(&message)?;
    // <-- HDR, SAr1, KEr
    let received = handshake.receive_ike_message()?;
    info!("got response: {received:?}");
    let notifs = received.notifications();
    for n in notifs {
        if n.data.is_error() {
            bail!("error notification: {:?}", n.data);
        }
    }
    // TODO(eta): We should check the SA here but also there's not much point, since we only gave
    //            one option :p
    let ke = received
        .key_exchange()
        .ok_or_else(|| anyhow!("no key exchange payload"))?;
    let nonce_received = received
        .nonce()
        .ok_or_else(|| anyhow!("no nonce payload"))?;
    let keying_material = Sha1KeyingMaterial::generate(
        &nonce,
        &nonce_received,
        received.initiator_spi(),
        received.responder_spi(),
        &dh_key,
        ke.dh_public
            .clone()
            .try_into()
            .map_err(|_| anyhow!("improperly sized KE payload"))?,
    );
    info!("generated keying material!");

    // ESP SA size is 4
    let mut child_sa_spi = vec![0; 4];
    rand::thread_rng().fill_bytes(&mut child_sa_spi);

    let ts_payload = vec![
        IkeTrafficSelector::Ipv4 {
            protocol: 0,
            start: "0.0.0.0:0".parse().unwrap(),
            end: "255.255.255.255:65535".parse().unwrap(),
        },
        IkeTrafficSelector::Ipv6 {
            protocol: 0,
            start: "[::]:0".parse().unwrap(),
            end: "[ffff:ffff:ffff:ffff:ffff:ffff:ffff:ffff]:65535"
                .parse()
                .unwrap(),
        },
    ];
    // HDR, SK { IDi, IDr, CP, SAi2, TSi, TSr } -->
    // self.set_identification(IDI,ID_RFC822_ADDR,'0' + self.imsi + '@nai.epc.mnc' + self.mnc + '.mcc' + self.mcc + '.3gppnetwork.org')
    let response = received
        .respond_with_auth()
        .with_payload(IkePayload::new_idi(IkeIdentificationData::Rfc822Addr(
            "0000000000000000@nai.epc.mnc020.mcc234.3gppnetwork.org".into(),
        )))
        .with_payload(IkePayload::new_idr(IkeIdentificationData::Fqdn(
            "ims".into(),
        )))
        .with_payload(IkePayload::new_cp(IkeConfiguration::request([
            ConfigurationAttrType::INTERNAL_IP4_ADDRESS,
            ConfigurationAttrType::INTERNAL_IP4_DNS,
            ConfigurationAttrType::INTERNAL_IP6_ADDRESS,
            ConfigurationAttrType::INTERNAL_IP6_DNS,
            ConfigurationAttrType::P_CSCF_IP4_ADDRESS,
            ConfigurationAttrType::P_CSCF_IP6_ADDRESS,
        ])))
        .with_payload(IkePayload::new_sa(vec![IkeSaProposal::new(
            IpsecProtocol::Esp,
        )
        .with_spi(child_sa_spi)
        .with_encr(EncrTransform::ENCR_AES_CBC, Some(128))
        .with_integ(IntegTransform::AUTH_HMAC_SHA1_96)
        .with_esn(EsnTransform::None)]))
        .with_payload(IkePayload::new_tsi(ts_payload.clone()))
        .with_payload(IkePayload::new_tsr(ts_payload.clone()))
        .as_bytes_encrypted(&keying_material)
        .unwrap();

    handshake.send_bytes(response)?;

    Ok(())
}
