//! Bad OpenSSL-based DH implementation, since I don't know what I'm doing.

use log::info;
use openssl::bn::BigNum;
use openssl::dh::Dh;
use openssl::pkey::Private;

pub(crate) struct DiffieKey2048 {
    inner: Dh<Private>,
}

impl DiffieKey2048 {
    pub(crate) fn new() -> Self {
        let prime = BigNum::get_rfc3526_prime_2048().unwrap();
        let generator = BigNum::from_u32(2).unwrap();
        let dh = Dh::from_pqg(prime, None, generator).unwrap();
        Self {
            inner: dh.generate_key().unwrap(),
        }
    }

    pub(crate) fn public_key(&self) -> [u8; 256] {
        self.inner.public_key().to_vec().try_into().unwrap()
    }

    pub(crate) fn derive_secret(&self, other_part: [u8; 256]) -> [u8; 256] {
        let mut other_bn = BigNum::new().unwrap();
        other_bn.copy_from_slice(&other_part).unwrap();
        self.inner
            .compute_key(&other_bn)
            .unwrap()
            .try_into()
            .unwrap()
    }
}
